Python XMLSERVICE Toolkit
=========================

:rotating_light: This project has moved to GitHub. The new repository is [here](https://github.com/IBM/python-itoolkit). :rotating_light: